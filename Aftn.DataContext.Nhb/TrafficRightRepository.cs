﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aftn.Domain;

namespace Aftn.DataContext.Nhb
{
    public class TrafficRightRepository : ITrafficRightRepository
    {
        protected Repository<TrafficRight> Repository = new Repository<TrafficRight>();

        public IQueryable<TrafficRight> All()
        {
            return Repository.All();
        }
    }
}
