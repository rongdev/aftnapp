﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;

namespace Aftn.DataContext.Nhb
{
    public class Repository<T> : IRepository<T>
    {
        private ISession session = NHibernateHelper.GetCurrentSession();

        public bool Add(T entity)
        {
            using (var transaction = session.BeginTransaction())
            {
                try
                {
                    session.Save(entity);
                    transaction.Commit();
                    return true;
                }
                catch
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public IQueryable<T> All()
        {
            return session.Query<T>();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            NHibernateQueryHandler<T> queryHandler = new NHibernateQueryHandler<T>();

            queryHandler.AddCriteria(expression);

            return queryHandler.GetList();
        }

        public void Save()
        {
            
        }

        public T Single(int id)
        {
            return session.Get<T>(id);
        }

        public T Single(Expression<Func<T, bool>> where)
        {
            NHibernateQueryHandler<T> queryHandler = new NHibernateQueryHandler<T>();
            queryHandler.AddCriteria(where);

            return queryHandler.GetList().FirstOrDefault();
        }
    }
}
