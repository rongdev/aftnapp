﻿using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Aftn.DataContext.Nhb
{
    public class NHibernateQueryHandler<T>
    {
        private readonly ISession _session = NHibernateHelper.GetCurrentSession(); //DependencyResolver.Resolve<ISessionManager>().OpenSession();
        private readonly IList<Expression<Func<T, bool>>> _criterialList;

        private IQueryable<T> _query;

        private int _pageIndex = 1;
        private int _pageSize = int.MaxValue;

        public NHibernateQueryHandler()
        {
            _criterialList = new List<Expression<Func<T, bool>>>();
            _query = from item in _session.Linq<T>()
                     select item;
        }

        /// <summary>
        /// 添加Where条件
        /// </summary>
        /// <param name="lambdaFunc"></param>
        /// <returns></returns>
        public NHibernateQueryHandler<T> AddCriteria(Expression<Func<T, bool>> lambdaFunc)
        {
            _criterialList.Add(lambdaFunc);
            return this;
        }

        /// <summary>
        /// 返回数据列表
        /// </summary>
        /// <returns></returns>
        public IList<T> GetList()
        {
            foreach (var criterion in _criterialList)
            {
                _query = _query.Where(criterion);
            }

            _query = _query.Skip((_pageIndex - 1) * _pageSize).Take(_pageSize);

            return _query.ToList();
        }

        /// <summary>
        /// 设置分页的页码
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public NHibernateQueryHandler<T> SetPageIndex(int index)
        {
            _pageIndex = index;
            return this;
        }

        /// <summary>
        /// 设置分页的页面大小
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public NHibernateQueryHandler<T> SetPageSize(int size)
        {
            _pageSize = size;
            return this;
        }

        /// <summary>
        /// 计算返回的数据数量
        /// </summary>
        /// <returns></returns>
        public int GetCount()
        {
            foreach (var criterion in _criterialList)
            {
                _query = _query.Where(criterion);
            }
            return _query.Count();
        }

        /// <summary>
        /// 升序(ASC)排序
        /// </summary>
        /// <param name="keySlec"></param>
        /// <returns></returns>
        public NHibernateQueryHandler<T> OrderBy(Expression<Func<T, object>> keySlec)
        {
            _query = _query.OrderBy(keySlec);

            return this;
        }

        /// <summary>
        /// 降序(DESC)排序
        /// </summary>
        /// <param name="keySlec"></param>
        /// <returns></returns>
        public NHibernateQueryHandler<T> OrderByDesc(Expression<Func<T, object>> keySlec)
        {
            _query = _query.OrderByDescending(keySlec);
            return this;
        }

    }
}
