﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aftn.Domain
{
    public class TrafficRightEffect
    {
        /// <summary>
		/// Id
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }
        /// <summary>
        /// TrafficRightId
        /// </summary>
        public virtual int TrafficRightId
        {
            get;
            set;
        }
        /// <summary>
        /// EffectDate
        /// </summary>
        public virtual DateTime EffectDate
        {
            get;
            set;
        }
    }
}
