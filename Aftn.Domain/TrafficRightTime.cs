﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aftn.Domain
{
    public class TrafficRightTime
    {
        /// <summary>
		/// Id
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }
        /// <summary>
        /// TrafficRightId
        /// </summary>
        public virtual int TrafficRightId
        {
            get;
            set;
        }
        /// <summary>
        /// Airport
        /// </summary>
        public virtual string Airport
        {
            get;
            set;
        }
        /// <summary>
        /// DepartureTime
        /// </summary>
        public virtual DateTime? DepartureTime
        {
            get;
            set;
        }
        /// <summary>
        /// ArrivalTime
        /// </summary>
        public virtual DateTime? ArrivalTime
        {
            get;
            set;
        }
    }
}
