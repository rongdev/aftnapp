﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aftn.Domain
{
    public class TrafficRight
    {
        /// <summary>
		/// Id
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }

        /// <summary>
        /// FlightNumber
        /// </summary>
        public virtual string FlightNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Schedule
        /// </summary>
        public virtual string Schedule
        {
            get;
            set;
        }

        /// <summary>
        /// RouteArea
        /// </summary>
        public virtual int RouteArea
        {
            get;
            set;
        }

        /// <summary>
        /// ResolveSource
        /// </summary>
        public virtual int ResolveSource
        {
            get;
            set;
        }

        /// <summary>
        /// ApprovalType
        /// </summary>
        public virtual int ApprovalType
        {
            get;
            set;
        }

        /// <summary>
        /// ResolveTime
        /// </summary>
        public virtual DateTime ResolveTime
        {
            get;
            set;
        }
        
        /// <summary>
        /// Remark
        /// </summary>
        public virtual string Remark
        {
            get;
            set;
        }
        
    }
}
