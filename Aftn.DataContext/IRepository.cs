﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Aftn.DataContext
{
    public interface IRepository<T>
    {
        T Single(Expression<Func<T, bool>> where);

        T Single(int id);

        bool Add(T entity);

        IQueryable<T> All();

        IEnumerable<T> Find(Expression<Func<T, bool>> expression);

        void Save();
    }
}
