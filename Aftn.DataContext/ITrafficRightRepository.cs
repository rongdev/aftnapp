﻿using Aftn.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aftn.DataContext
{
    public interface ITrafficRightRepository
    {
        IQueryable<TrafficRight> All();
    }
}
