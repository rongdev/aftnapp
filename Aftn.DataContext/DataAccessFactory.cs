﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aftn.DataContext
{
    public class DataAccessFactory
    {
        private static Type _dataAccessType;

        public static IDataAccessManager GetManager()
        {
            if (_dataAccessType == null)
            {
                var dalTypeName = ConfigurationManager.AppSettings["DataAccessManagerType"];
                if (!string.IsNullOrEmpty(dalTypeName))
                {
                    _dataAccessType = Type.GetType(dalTypeName);
                }
                else
                {
                    throw new NullReferenceException("DataAccessManagerType");
                }
                if (_dataAccessType == null)
                {
                    throw new ArgumentException(string.Format("Type {0} could not be found", dalTypeName));
                }
            }
            return (IDataAccessManager)Activator.CreateInstance(_dataAccessType);
        }
    }
}
