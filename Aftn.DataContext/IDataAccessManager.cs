﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aftn.DataContext
{
    public interface IDataAccessManager : IDisposable
    {
        T GetProvider<T>() where T : class;
    }
}
